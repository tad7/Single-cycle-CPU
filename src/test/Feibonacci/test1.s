	.file	"test1.c"
	.option nopic
	.text
	.align	1
	.globl	main
	.type	main, @function
main:
	addi	sp,sp,-64
	sw	s0,60(sp)
	addi	s0,sp,64
	sw	zero,-56(s0)
	sw	zero,-52(s0)
	sw	zero,-48(s0)
	sw	zero,-44(s0)
	sw	zero,-40(s0)
	sw	zero,-36(s0)
	sw	zero,-20(s0)
	j	.L2
.L8:
	lw	a5,-20(s0)
	beqz	a5,.L3
	lw	a4,-20(s0)
	li	a5,1
	bne	a4,a5,.L4
.L3:
	lw	a5,-20(s0)
	slli	a5,a5,2
	addi	a4,s0,-16
	add	a5,a4,a5
	li	a4,1
	sw	a4,-40(a5)
	j	.L5
.L4:
	lw	a5,-20(s0)
	addi	a5,a5,-1
	slli	a5,a5,2
	addi	a4,s0,-16
	add	a5,a4,a5
	lw	a4,-40(a5)
	lw	a5,-20(s0)
	addi	a5,a5,-2
	slli	a5,a5,2
	addi	a3,s0,-16
	add	a5,a3,a5
	lw	a5,-40(a5)
	add	a4,a4,a5
	lw	a5,-20(s0)
	slli	a5,a5,2
	addi	a3,s0,-16
	add	a5,a3,a5
	sw	a4,-40(a5)
.L5:
	lw	a5,-20(s0)
	slli	a5,a5,2
	addi	a4,s0,-16
	add	a5,a4,a5
	lw	a4,-40(a5)
	li	a5,5
	ble	a4,a5,.L6
	lw	a4,-20(s0)
	li	a5,1
	bgt	a4,a5,.L9
.L6:
	lw	a5,-20(s0)
	addi	a5,a5,1
	sw	a5,-20(s0)
.L2:
	lw	a4,-20(s0)
	li	a5,5
	ble	a4,a5,.L8
	j	.L7
.L9:
	nop
.L7:
	lw	a4,-44(s0)
	lw	a5,-40(s0)
	or	a5,a4,a5
	sw	a5,-24(s0)
	lw	a4,-44(s0)
	lw	a5,-40(s0)
	and	a5,a4,a5
	sw	a5,-28(s0)
	lw	a4,-44(s0)
	lw	a5,-40(s0)
	xor	a5,a4,a5
	sw	a5,-32(s0)
	nop
	lw	s0,60(sp)
	addi	sp,sp,64
	jr	ra
	.size	main, .-main
	.ident	"GCC: (GNU MCU Eclipse RISC-V Embedded GCC, 64-bit) 8.2.0"

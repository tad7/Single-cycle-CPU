`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2023/04/27 17:45:08
// Design Name: 
// Module Name: id_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module id_tb;
   reg [31:0] Instr;
   wire [4:0] rs1;
   wire [4:0] rs2;
   wire [4:0] rd;
   wire [31:0] imm;
   wire sPC,ALUsrc1,ALUsrc2,MemWr,MemOrReg,Flag_jalr;
   wire RegWr;
   wire [1:0] ext_rs2;
   wire [3:0] ALUctr;
   wire [2:0] ALU_Condition;
   wire [2:0] ext_M;
   
   initial begin
      #10
      Instr<=32'hfe010113;
      #10
      Instr<=32'h00a00793;
      #10
      Instr<=32'h00812e23;
      #10
      Instr<=32'h0200006f;
      $finish;
   end
   
   Instruction_Decoder id(Instr,rs1,rs2,rd,imm,sPC,RegWr,ext_rs2,ALUsrc1,ALUsrc2,ALUctr,ALU_Condition,MemWr,ext_M,MemOrReg,Flag_jalr);
endmodule

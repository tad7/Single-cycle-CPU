`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2023/04/26 23:55:38
// Design Name: 
// Module Name: cpu_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module cpu_tb;
   reg clk;
   reg rst;
   wire [31:0] ALU_Result;
   
   top cup(clk,rst,ALU_Result);
    
   initial begin
      #10
      clk=1'b0;
      rst=1'b1;
      #5
      rst=1'b0;
   end
   
   always #10 clk=~clk;
   
endmodule

`include "define.v"

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2023/04/26 14:39:46
// Design Name: 
// Module Name: alu_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module alu_tb;
    reg ALUsrc1;  
    reg ALUsrc2;  
    reg [31 :0] busA;
    reg [31 :0] busB;  
    reg [31:0] PC;
    reg [2:0] ALU_Condition;  
    reg [31:0] imm;  
    reg [3:0] ALUctr;   
    
    wire zero;
    wire [31:0] ALU_Result;
    
   reg clk;
   initial begin
      clk=1'b0;
   end
   
   always  #10 clk=~clk;
	
   always @(posedge clk) begin 
      ALUsrc1=1'b1;
      ALUsrc2=1'b0;
      busA=32'h4;
      busB=32'h8;
      PC=32'h3;
      ALU_Condition=`Condition_nocompare;
      imm=32'h9;
      ALUctr=`ALU_add;
      #10
      ALUsrc1=1'b1;
      ALUsrc2=1'b0;
      busA=32'h9;
      busB=32'h2;
      PC=32'h3;
      ALU_Condition=`Condition_nocompare;
      imm=32'h9;
      ALUctr=`ALU_sub;
      #10
      ALUsrc1=1'b1;
      ALUsrc2=`ALUsrc2_imm;
      busA=32'h4;
      busB=32'h8;
      PC=32'h3;
      ALU_Condition=`Condition_nocompare;
      imm=32'h9;
      ALUctr=`ALU_and;
      #10
      ALUsrc1=`ALUsrc1_PC;
      ALUsrc2=1'b0;
      busA=32'h4;
      busB=32'h8;
      PC=32'h3;
      ALU_Condition=`Condition_nocompare;
      imm=32'h9;
      ALUctr=`ALU_or;
      #10
      ALUsrc1=1'b1;
      ALUsrc2=1'b0;
      busA=32'h4;
      busB=32'h8;
      PC=32'h3;
      ALU_Condition=`Condition_nocompare;
      imm=32'h9;
      ALUctr=`ALU_xor;
      #10 
      ALUsrc1=1'b1;
      ALUsrc2=1'b0;
      busA=32'h4;
      busB=32'h8;
      PC=32'h3;
      ALU_Condition=`Condition_GreaterOrEqual;
      imm=32'h9;
      ALUctr=4'h5;
      #10 
      ALUsrc1=1'b1;
      ALUsrc2=1'b0;
      busA=32'h5;
      busB=32'h0;
      PC=32'h3;
      ALU_Condition=`Condition_LowerOrEqual;
      imm=32'h9;
      ALUctr=`ALU_compare_s;
      #10 
      ALUsrc1=1'b1;
      ALUsrc2=1'b0;
      busA=32'h5;
      busB=32'h0;
      PC=32'h3;
      ALU_Condition=3'h4;
      imm=32'h9;
      ALUctr=`ALU_compare_s;
      #10 
      ALUsrc1=1'b1;
      ALUsrc2=1'b0;
      busA=32'h4;
      busB=32'h8;
      PC=32'h3;
      ALU_Condition=`Condition_nocompare;
      imm=32'h9;
      ALUctr=4'h7;
      #10 
      ALUsrc1=1'b1;
      ALUsrc2=1'b0;
      busA=32'ha;
      busB=32'h3;
      PC=32'h3;
      ALU_Condition=`Condition_nocompare;
      imm=32'h9;
      ALUctr=4'h8;
      #10 
      ALUsrc1=1'b1;
      ALUsrc2=1'b0;
      busA=32'ha;
      busB=32'h5;
      PC=32'h3;
      ALU_Condition=`Condition_nocompare;
      imm=32'h9;
      ALUctr=4'h9;
      #10 
      ALUsrc1=1'b1;
      ALUsrc2=1'b0;
      busA=32'h4;
      busB=32'h8;
      PC=32'h3;
      ALU_Condition=`Condition_nocompare;
      imm=32'h9;
      ALUctr=4'ha;
      $finish;
   end
   
   Alu alu(ALUsrc1,ALUsrc2,busA,busB,PC,ALU_Condition,imm,ALUctr,zero,ALU_Result);
endmodule

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2023/04/20 11:17:46
// Design Name: 
// Module Name: define
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

`define Enable 1'b1
`define Disable 1'b0
`define Nooutput_32bit 32'b0
`define Nooutput_5bit 5'b0;

//根据opcode区分指令类型
`define R_Type 7'b0110011
`define I_Load 7'b0000011
`define I_ALU 7'b0010011
`define I_jalr 7'b1100111
`define S_Type 7'b0100011
`define B_Type 7'b1100011
`define U_lui 7'b0110111
`define U_auipc 7'b0010111
`define J_Type 7'b1101111
    
//指令划分
`define RS1 Instr[19:15]
`define RS2 Instr[24:20]
`define RD Instr[11:7]
`define funct7 Instr[31:25]
`define funct3 Instr[14:12]
`define opcode Instr[6:0]

//imm的扩展选项
`define ext_immI {{20{Instr[31]}},Instr[31:20]}
`define ext_immS {{20{Instr[31]}},Instr[31:25],Instr[11:7]}
`define ext_immB {{20{Instr[31]}},Instr[7],Instr[30:25],Instr[11:8],1'b0}
`define ext_immU {Instr[31:12],12'b0}
`define ext_immJ {{12{Instr[31]}},Instr[19:12],Instr[20],Instr[30:21],1'b0}
`define ext_shamt {27'b0,Instr[24:20]}

//控制信号sPC的选项
`define sPC_add4 1'b0;
`define sPC_other 1'b1;

//控制信号ext_rs2的选项
`define ext_rs2_all 2'b00  //不截取
`define ext_rs2_8bit 2'b10  //截取8bit并进行0扩展
`define ext_rs2_16bit 2'b11  //截取16bit并进行0扩展

//控制信号ALUsrc1,2的选项
`define ALUsrc1_PC 1'b0
`define ALUsrc1_rs1 1'b1
`define ALUsrc2_imm 1'b1
`define ALUsrc2_rs2 1'b0

//控制信号ALUctr的选项
`define ALU_add 4'b0  //0
`define ALU_sub 4'b1  //1
`define ALU_and 4'b10  //2
`define ALU_or 4'b11  //3
`define ALU_xor 4'b100  //4
`define ALU_compare_u 4'b101  //5
`define ALU_compare_s 4'b110  //6
`define ALU_sl 4'b111  //7
`define ALU_srl 4'b1000  //8
`define ALU_sra 4'b1001  //9
`define ALU_input1_add4 4'b1010  //10

//控制信号ALU_condition的选项
`define Condition_nocompare 3'b000  //不进行条件判断
`define Condition_Greater 3'b001
`define Condition_Lower 3'b011
`define Condition_GreaterOrEqual 3'b010
`define Condition_LowerOrEqual 3'b110
`define Condition_Equal  3'b100
`define Condition_Notequal 3'b111

//控制信号ext_M的选项
`define ext_M_all 3'b000  //不截取，直接输出
`define ext_M_8bit_s 3'b111  //截取8bit并符号扩展
`define ext_M_16bit_s 3'b110  //截取16bit并符号扩展
`define ext_M_8bit_z 3'b101 //截取8bit并零扩展
`define ext_M_16bit_z 3'b100  //截取16bit并零扩展

//控制信号MemOrReg的选项
`define MemOrReg_Mem 1'b1
`define MemOrReg_ALU 1'b0


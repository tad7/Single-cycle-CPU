`include "define.v"


module DataCache(
    input wire MemWr,
    input wire [2:0] ext_M,
    input wire [31:0] Addr,
    input wire [31:0] DataIn,
    output reg [31:0] Data
    );
    
    reg [31:0] D [0:1023];
    wire [9:0] addr;

    assign addr=Addr[11:2];
    
    always@(*) begin
        if(MemWr) begin  //д��
        #1.2
           D[addr]<=DataIn;
           Data<=32'b0;
        end
        case(ext_M)  //����
            `ext_M_all:begin  Data<=D[addr][31:0]; end
            `ext_M_8bit_s:begin  Data<={{24{D[addr][7]}},D[addr][7:0]}; end
            `ext_M_16bit_s:begin  Data<={{16{D[addr][15]}},D[addr][15:0]}; end
            `ext_M_8bit_z:begin Data<={{24{1'b0}},D[addr][15:0]}; end
            `ext_M_16bit_z:begin  Data<={{16{1'b0}},D[addr][15:0]}; end
        endcase
    end
      
endmodule
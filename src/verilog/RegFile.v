`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2023/04/22 18:37:52
// Design Name: 
// Module Name: RegFile
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "define.v"

module RegFile(
    input wire reset,
    input wire clk,
    input wire RegWr,
    input wire [4:0] rd,
    input wire [4:0] ra,
    input wire [4:0] rb,
    input wire MemOrReg,
    input wire [31:0] Mem,
    input wire [31:0] Reg,
    output reg [31:0] busA,
    output reg [31:0] busB 
    );
    integer i;
    
    reg [31:0] R [31:0];
    
    always @(reset) begin
        R[0][31:0]<=32'b0;
        R[1][31:0]<=32'b0;
        R[2][31:0]<=32'h7ffffff0;
        R[3][31:0]<=32'h10000000;
        for(i=4;i<=31;i=i+1)begin
            R[i]=32'b0;
        end
    end
    
    always @(posedge clk)begin  //д��
        if(RegWr==1'b1 && rd!=5'b0)
           begin  
                case(MemOrReg)
                `MemOrReg_Mem:begin R[rd]<=Mem; end
                `MemOrReg_ALU:begin R[rd]<=Reg; end
                endcase
           end
    end
    
    always@(*)begin  //����
       if(reset) begin
           busA<=32'b0;
           busB<=32'b0;
        end
        else begin
           busA<=R[ra];
           busB<=R[rb];
        end
    end
    
endmodule

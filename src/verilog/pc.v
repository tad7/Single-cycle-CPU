`include "define.v"

module pc(
    input wire clk,
    input wire rst,
	input wire sPC,
	input wire zero,
	input wire Flag_jalr,
	input wire [31:0] imm,
	input wire [31:0] Addr_rs1,
    output reg [31:0] Addr           
);

	always@(posedge clk)begin
	    #2
        if (rst == 1'b1) begin  //��λ
            Addr <= 32'b0;
        end 
		else begin
			if (sPC ==`Enable && zero ==`Enable) begin
				if (Flag_jalr == `Enable)begin
					Addr <= Addr_rs1 + imm ;
					Addr <= {Addr[31:1],1'b0};
				end
				else begin
					Addr <= Addr + imm; 
				end
			end
		    else begin
		       Addr <= Addr + 32'b100;
		    end
	    end
    end 	
		
endmodule	
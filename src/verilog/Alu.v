`include "define.v"

module Alu(
    input wire Alu_SrcA,  //输入端1位选
    input wire Alu_SrcB,  //输入端2位选
    input wire [31 :0] ReadData1,   //操作数1-rs1寄存器读取数据
    input wire [31 :0] ReadData2,   //操作数2-rs2寄存器读取数据
    input wire [31:0] PC,   //当前地址用于计算跳转地址
    input wire [2:0] AluCondition,  //判断条件
    input wire [31:0] extend,  //扩展后立即数
    input wire [3:0] Alu_Op,   //操作-ALU功能码
    output  reg zero,
    output  reg[31 :0] Alu_Result  //结果
    );
    reg [31:0] Alu_Src1;
    reg [31:0] Alu_Src2;
    
    always@(*)
    begin
        #2
        //if(Alu_SrcB == 0) Alu_Src2=ReadData2;
        //else Alu_Src2=extend;
        Alu_Src2 = (Alu_SrcB == 0) ? ReadData2 : extend;
        Alu_Src1 = (Alu_SrcA == 1) ? ReadData1 : PC;
        //if(AluCondition==`Condition_nocompare) zero<=1;  //只要ALU的功能不是比较，就都将zero信号置1
        if(Alu_Op==4'b0000) begin       //加法 
            Alu_Result<=Alu_Src1+Alu_Src2;
            zero<=1;
        end
        else if(Alu_Op==4'b0001) begin //减法
            Alu_Result<=Alu_Src1-Alu_Src2;
            zero<=1;
        end
        else if(Alu_Op==4'b0010) begin //按位与
            Alu_Result<=Alu_Src1&Alu_Src2; zero<=1; end
        else if(Alu_Op==4'b0011)begin  //按位或
            Alu_Result<=(Alu_Src1|Alu_Src2); zero<=1; end
        else if(Alu_Op==4'b0100) begin //按位异或
            Alu_Result<=(Alu_Src1^Alu_Src2);  zero<=1; end
        //else if(Alu_Op==4'b0101||Alu_Op==4'b0110)begin //比较
        else if(Alu_Op==`ALU_compare_u) begin  //无符号
            Alu_Result<=(($unsigned(Alu_Src1)<$unsigned(Alu_Src2))&&AluCondition==`Condition_nocompare)?{31'b0,1'b1}:32'b0;
                case(AluCondition)
                   `Condition_Greater: zero<=($unsigned(Alu_Src1)>$unsigned(Alu_Src2))?1:0;
                   `Condition_Lower: zero<=($unsigned(Alu_Src1)<$unsigned(Alu_Src2))?1:0;
                   `Condition_GreaterOrEqual: zero<=($unsigned(Alu_Src1)>=$unsigned(Alu_Src2))?1:0;
                   `Condition_LowerOrEqual: zero<=($unsigned(Alu_Src1)<=$unsigned(Alu_Src2))?1:0;
                   3'h4: zero<=($unsigned(Alu_Src1)==$unsigned(Alu_Src2))?1:0;
                   `Condition_Notequal: zero<=($unsigned(Alu_Src1)!=$unsigned(Alu_Src2))?1:0;
                   3'h0: zero<=1;
                endcase
         end
         else if(Alu_Op==`ALU_compare_s) begin  //有符号
                Alu_Result<=(($signed(Alu_Src1)<$signed(Alu_Src2))&&AluCondition==`Condition_nocompare)?{31'b0,1'b1}:32'b0;
                case(AluCondition)
                   `Condition_Greater: zero<=($signed(Alu_Src1)>$signed(Alu_Src2))?1:0;
                   `Condition_Lower: zero<=($signed(Alu_Src1)<$signed(Alu_Src2))?1:0;
                   `Condition_GreaterOrEqual: zero<=($signed(Alu_Src1)>=$signed(Alu_Src2))?1:0;
                   `Condition_LowerOrEqual: zero<=($signed(Alu_Src1)<=$signed(Alu_Src2))?1:0;
                   3'h4: zero<=($signed(Alu_Src1)!=$signed(Alu_Src2))?0:1;
                   `Condition_Notequal: zero<=($signed(Alu_Src1)!=$signed(Alu_Src2))?1:0;
                   3'h0: zero<=1;
                endcase
        end
        //end
        else if(Alu_Op==4'b0111)begin  //逻辑左移
            Alu_Result<=Alu_Src1<<Alu_Src2; zero<=1; end
        else if(Alu_Op==4'b1000) begin //逻辑右移
            Alu_Result<=Alu_Src1>>Alu_Src2; zero<=1; end
        else if(Alu_Op==4'b1001) begin //算数右移
            Alu_Result<=Alu_Src1>>>Alu_Src2;  zero<=1; end
        else if(Alu_Op==4'b1010) begin  //add4
            Alu_Result=Alu_Src1+4; zero<=1; 
        end
    end
endmodule


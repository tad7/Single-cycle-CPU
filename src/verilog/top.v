
module top(
    input wire clk,
    input wire rst,
    output wire [31:0] ALU_result
    );
    
    wire sPC,zero,MemWr,MemOrReg,ALUsrc1,ALUsrc2,RegWr,Flag_jalr;
    wire [2:0] ALU_Condition;
	wire [2:0]  ext_M;
	wire [3:0] ALUctr;
	wire [1:0] ext_rs2;
	wire [4:0]  rs1;
	wire [4:0]  rs2;
	wire [4:0]  rd;
	wire [31:0] Instr;
	wire [31:0] imm;
	wire [31:0] Addr;
	wire [31:0] busA;
	wire [31:0] busB;
	wire [31:0] Mem;

	pc pc1(
	.clk(clk),
	.rst(rst),
	.sPC(sPC),
	.zero(zero),
	.Flag_jalr(Flag_jalr),
	.imm(imm),
	.Addr_rs1(busA),
	.Addr(Addr));
	
	InstrCache InstrCache(
	.Addr(Addr),
    .Instr(Instr));
    
    Instruction_Decoder Instruction_Decoder(
	.Instr(Instr), 
	.rs1(rs1),
	.rs2(rs2),
	.rd(rd),
	.imm(imm),  
	.sPC(sPC),
	.RegWr(RegWr),
	.ext_rs2(ext_rs2),
	.ALUsrc1(ALUsrc1),
	.ALUsrc2(ALUsrc2),
	.ALUctr(ALUctr),
	.ALU_Condition(ALU_Condition),
	.MemWr(MemWr),
	.ext_M(ext_M),
	.MemOrReg(MemOrReg), 
	.Flag_jalr(Flag_jalr));
	
	Alu Alu(
	.Alu_SrcA(ALUsrc1),  
    .Alu_SrcB(ALUsrc2), 
    .ReadData1(busA),  
    .ReadData2(busB),   
    .PC(Addr),   
    .AluCondition(ALU_Condition),  
    .extend(imm),  
    .Alu_Op(ALUctr),   
    .zero(zero),
    .Alu_Result(ALU_result));
	    
	DataCache DataCache(
    .MemWr(MemWr),
    .ext_M(ext_M),
    .Addr(ALU_result),
    .DataIn(busB),
    .Data(Mem));
	
	RegFile RegFile(
	.reset(rst),
	.clk(clk),
	.RegWr(RegWr),
    .rd(rd),
    .ra(rs1),
    .rb(rs2),
    .MemOrReg(MemOrReg),
    .Mem(Mem),
    .Reg(ALU_result),
    .busA(busA),
    .busB(busB));

endmodule
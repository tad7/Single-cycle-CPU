`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2023/04/20 11:03:08
// Design Name: 
// Module Name: Instruction_Decoder
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

`include "define.v"
module Instruction_Decoder(
input wire [31:0] Instr,  //输入

output reg [4:0] rs1,
output reg [4:0] rs2,
output reg [4:0] rd,
output reg [31:0] imm,  //指令划分

output reg sPC,
output reg RegWr,
output reg [1:0] ext_rs2,
output reg ALUsrc1,
output reg ALUsrc2,
output reg [3:0] ALUctr,
output reg [2:0] ALU_Condition,
output reg MemWr,
output reg [2:0] ext_M,
output reg MemOrReg, 
output reg Flag_jalr   //控制信号
    );
    
    always @(*) begin
       case (Instr[6:0])
          `R_Type:begin  
              rs2<=`RS2;
              rs1<=`RS1;
              rd<=`RD;
              imm<=`Nooutput_32bit;
              sPC<=`sPC_add4;
              RegWr<=`Enable;
              ext_rs2<=`ext_rs2_all; 
              ALUsrc1<=`ALUsrc1_rs1;
              ALUsrc2<=`ALUsrc2_rs2;
              MemWr=`Disable;
              ext_M<=`ext_M_all;
              MemOrReg<=`MemOrReg_ALU;
              Flag_jalr<=`Disable;
              case(Instr[31:25])
                 7'b0:begin
                    case(Instr[14:12])
                       3'b0:begin  ALUctr<=`ALU_add; ALU_Condition<=`Condition_nocompare; end   //add
                       3'b10:begin  //slt
                          ALUctr<=`ALU_compare_s;
                          ALU_Condition<=`Condition_Lower;
                       end
                       3'b11:begin  //sltu
                          ALUctr<=`ALU_compare_u;
                          ALU_Condition<=`Condition_Lower;
                       end
                       3'b100:begin  ALUctr<=`ALU_xor; ALU_Condition<=`Condition_nocompare; end   //xor
                       3'b101:begin  ALUctr<=`ALU_srl; ALU_Condition<=`Condition_nocompare; end   //srl
                       3'b110:begin  ALUctr<=`ALU_or; ALU_Condition<=`Condition_nocompare; end   //or
                       3'b111:begin  ALUctr<=`ALU_and; ALU_Condition<=`Condition_nocompare; end   //and
                    endcase
                 end
                 7'b0100000:begin
                    case(`funct3)
                       3'b0:begin  ALUctr<=`ALU_sub; ALU_Condition<=`Condition_nocompare; end  //sub
                       3'b101:begin   ALUctr<=`ALU_sra; ALU_Condition<=`Condition_nocompare; end  //sra
                    endcase
                 end
              endcase
          end
          `I_Load:begin
              rs1<=`RS1;
              rs2<=`Nooutput_32bit;
              rd<=`RD;
              imm<=`ext_immI;            
              sPC<=`sPC_add4;
              RegWr<=`Enable;
              ext_rs2<=`ext_rs2_all;
              ALUsrc1<=`ALUsrc1_rs1;
              ALUsrc2<=`ALUsrc2_imm;
              ALUctr<=`ALU_add;
              ALU_Condition<=`Condition_nocompare;
              MemWr=`Disable;
              MemOrReg<=`MemOrReg_Mem;
              Flag_jalr<=`Disable; 
              case(`funct3)
                 3'b000:begin ext_M<=`ext_M_8bit_s;   end   //lb
                 3'b001:begin ext_M<=`ext_M_16bit_s;   end   //lh
                 3'b010:begin ext_M<=`ext_M_all;   end  //lw
                 3'b100:begin ext_M<=`ext_M_8bit_z;  end  //lbu
                 3'b101:begin ext_M<=`ext_M_16bit_z;  end  //lhu
              endcase
          end
          `I_ALU:begin
              rs1<=`RS1;
              rs2<=`Nooutput_5bit;
              rd<=`RD;
              sPC<=`sPC_add4;
              RegWr<=`Enable;
              ext_rs2<=`ext_rs2_all;
              ALUsrc1<=`ALUsrc1_rs1;
              ALUsrc2<=`ALUsrc2_imm;
              MemWr<=`Disable;
              ext_M<=`ext_M_all;
              MemOrReg<=`MemOrReg_ALU;
              Flag_jalr<=`Disable;
              case(`funct3)
                 3'b000:begin imm<=`ext_immI; ALUctr<=`ALU_add; ALU_Condition<=`Condition_nocompare; end  //addi
                 3'b010:begin imm<=`ext_immI; ALUctr<=`ALU_compare_s; ALU_Condition<=`Condition_Lower; end  //slti
                 3'b011:begin imm<=`ext_immI; ALUctr<=`ALU_compare_u; ALU_Condition<=`Condition_Lower; end //sltiu
                 3'b100:begin imm<=`ext_immI; ALUctr<=`ALU_xor; ALU_Condition<=`Condition_nocompare; end //xori
                 3'b110:begin imm<=`ext_immI; ALUctr<=`ALU_or; ALU_Condition<=`Condition_nocompare; end //ori
                 3'b111:begin imm<=`ext_immI; ALUctr<=`ALU_and; ALU_Condition<=`Condition_nocompare; end //andi
                 3'b001:begin imm<=`ext_shamt; ALUctr<=`ALU_sl; ALU_Condition<=`Condition_nocompare; end //slli
                 3'b101:begin //srli和srai
                    imm<=`ext_shamt; 
                    case(`funct7)
                       7'b0100000:begin ALUctr<=`ALU_sra; ALU_Condition<=`Condition_nocompare; end  //srai
                       7'b0000000:begin ALUctr<=`ALU_srl; ALU_Condition<=`Condition_nocompare; end  //srli
                    endcase
                 end 
              endcase
          end
          `I_jalr:begin
             rs1<=`RS1;
             rs2<=`Nooutput_5bit;
             rd<=`RD;
             imm<=`ext_immI;
             sPC<=`sPC_other;
             RegWr<=`Enable;
             ext_rs2<=`ext_rs2_all;
             ALUsrc1<=`ALUsrc1_PC;
             ALUsrc2<=`ALUsrc2_rs2;
             ALUctr<=`ALU_input1_add4;
             ALU_Condition<=`Condition_nocompare;
             MemWr<=`Disable;
             ext_M<=`ext_M_all;
             MemOrReg<=`MemOrReg_ALU;
             Flag_jalr<=`Enable;
          end
          `S_Type:begin
             rs1<=`RS1;
             rs2<=`RS2;
             rd<=`Nooutput_5bit;
             imm<=`ext_immS;
             sPC<=`sPC_add4;
             RegWr<=`Disable;
             ALUsrc1<=`ALUsrc1_rs1;
             ALUsrc2<=`ALUsrc2_imm;
             ALUctr<=`ALU_add;
             ALU_Condition<=`Condition_nocompare;
             MemWr<=`Enable;
             ext_M<=`ext_M_all;
             MemOrReg<=`MemOrReg_ALU;
             Flag_jalr<=`Disable;
             case(`funct3)
                3'b000:begin ext_rs2<=`ext_rs2_8bit; end //sb
                3'b001:begin ext_rs2<=`ext_rs2_16bit; end //sh
                3'b010:begin ext_rs2<=`ext_rs2_all;  end //sw
             endcase
          end
          `B_Type:begin
             rs1<=`RS1;
             rs2<=`RS2;
             rd<=`Nooutput_5bit;
             imm<=`ext_immB;
             sPC<=`sPC_other;
             RegWr<=`Disable;
             ext_rs2<=`ext_rs2_all;
             ALUsrc1<=`ALUsrc1_rs1;
             ALUsrc2<=`ALUsrc2_rs2;
             MemWr<=`Disable;
             ext_M<=`ext_M_all;
             MemOrReg<=`MemOrReg_ALU;
             Flag_jalr<=`Disable;
             case(`funct3)
                3'b000:begin ALUctr<=`ALU_compare_s; ALU_Condition<=`Condition_Equal;  end  //beq
                3'b001:begin ALUctr<=`ALU_compare_s; ALU_Condition<=`Condition_Notequal; end  //bne
                3'b100:begin ALUctr<=`ALU_compare_s; ALU_Condition<=`Condition_Lower; end  //blt
                3'b101:begin ALUctr<=`ALU_compare_s; ALU_Condition<=`Condition_GreaterOrEqual; end  //bge
                3'b110:begin ALUctr<=`ALU_compare_u; ALU_Condition<=`Condition_Lower; end  //bltu
                3'b111:begin ALUctr<=`ALU_compare_u; ALU_Condition<=`Condition_GreaterOrEqual; end  //bgeu
             endcase
          end
          `U_lui:begin
             rs1<=`Nooutput_5bit;
             rs2<=`Nooutput_5bit;
             rd<=`RD;
             imm<=`ext_immU;
             sPC<=`sPC_add4;
             RegWr<=`Enable;
             ext_rs2<=`ext_rs2_all;
             ALUsrc1<=`ALUsrc1_rs1;  
             //此时x[rs1]存放的是0（因为规定RegFile地址为32'b0的单元应该存放32'b0）
             ALUsrc2<=`ALUsrc2_imm;
             ALUctr<=`ALU_add;
             ALU_Condition<=`Condition_nocompare;
             MemWr<=`Disable;
             ext_M<=`ext_M_all;
             MemOrReg<=`MemOrReg_ALU;
             Flag_jalr<=`Disable;
          end
          `U_auipc:begin
             rs1<=`Nooutput_5bit;
             rs2<=`Nooutput_5bit;
             rd<=`RD;
             imm<=`ext_immU;
             sPC<=`sPC_add4;
             RegWr<=`Enable;
             ext_rs2<=`ext_rs2_all;
             ALUsrc1<=`ALUsrc1_PC;  
             ALUsrc2<=`ALUsrc2_imm;
             ALUctr<=`ALU_add;
             ALU_Condition<=`Condition_nocompare;
             MemWr<=`Disable;
             ext_M<=`ext_M_all;
             MemOrReg<=`MemOrReg_ALU;
             Flag_jalr<=`Disable;
          end
          `J_Type:begin
             rs1<=`Nooutput_5bit;
             rs2<=`Nooutput_5bit;
             rd<=`RD;
             imm<=`ext_immJ;
             sPC<=`sPC_other;
             RegWr<=`Enable;
             ext_rs2<=`ext_rs2_all;
             ALUsrc1<=`ALUsrc1_PC;
             ALUsrc2<=`ALUsrc2_imm;  //此时ALU_input2无所谓输入什么,都不影响
             ALUctr<=`ALU_input1_add4;
             ALU_Condition<=`Condition_nocompare;
             MemWr<=`Disable;
             ext_M<=`ext_M_all;
             MemOrReg<=`MemOrReg_ALU;
             Flag_jalr<=`Disable;
          end
          default:begin  //其他指令（正常情况下不会发生）  //那么就什么也不做直接跳过该指令
             rs1<=`Nooutput_5bit;
             rs2<=`Nooutput_5bit;
             rd<=`Nooutput_5bit;
             imm<=`Nooutput_32bit;
             sPC<=`sPC_add4;
             RegWr<=`Disable;
             ext_rs2<=`ext_rs2_all;
             ALUsrc1<=`ALUsrc1_rs1;
             ALUsrc2<=`ALUsrc2_rs2; 
             ALUctr<=`ALU_add;
             ALU_Condition<=`Condition_nocompare;
             MemWr<=`Disable;
             ext_M<=`ext_M_all;
             MemOrReg<=`MemOrReg_ALU;
             Flag_jalr<=`Disable;
          end
       endcase   //end_of_case(opcode)
       //if((ALUctr!=`ALU_compare_s)&&(ALUctr!=`ALU_compare_u)) 
          //begin  ALU_Condition<=`Condition_nocompare;  end  //对于ALU非比较操作，一律将控制信号ALU_Condition设置为nocompare
    end   //end_of_always
endmodule

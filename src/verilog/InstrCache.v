
module InstrCache(
    input wire [31:0] Addr,
    output wire [31:0] Instr
    );
    wire [31:0] instr;
    dist_mem_gen_0 ins(Addr[11:2],instr);
    assign Instr = instr;
endmodule
# 单周期哈佛结构CPU

## 项目环境
设计语言：Verilog 硬件描述语言
仿真环境：Vivado 2018.3 版本

## 任务目标
课程设计要求：设计一个兼容 32 位 MIPS 或 RISC-V 指令集的单周期 CPU 核  
指令集： RISC-V 指令集（RV32I)  
设计目标:  
  1.使用哈佛结构的单周期 CPU
  2.支持 RV32I 基础整数指令集中除“状态与控制”类指令外的所有指令
  3.能够运行简单的小程序，且充分利用 RV32I 指令集。


## 参考资料
（1）RISC-V 手册 一本开源指令集的指南 DAVID PATTERSON, ANDREW WATERMAN 翻译:勾凌睿,黄成,刘志刚  
（2）康振邦-RISCV 指令集分类（译码版）_v4_2020.1.25  
（3）The RISC-V Instruction Set Manual Volume II: Privileged Architecture Document Version 20211203  
（4）The RISC-V Instruction Set Manual Volume I: Unprivileged ISA Document Version 20191213  
（5）所有课程 PPT

## 项目文件夹结构
|-project_1.zip (vivado 项目文件夹打包）    
|-计组大作业一报告.pdf  
|-src （源代码文件夹）  
&emsp;|-verilog （cpu设计文件源码文件夹）  
&emsp;&emsp;|-IP （IP核文件）  
&emsp;&emsp;|-testbench （TESTBENCH 文件源码文件夹）  
&emsp;&emsp;&emsp;|-alu_tb.v （ALU 测试文件）  
&emsp;&emsp;&emsp;|-cpu_tb.v （cpu 整体测试文件）  
&emsp;&emsp;&emsp;|-id_tb.v  （译码器测试文件）  
&emsp;&emsp;|-test （cpu整体测试小程序源码文件夹）  
&emsp;&emsp;&emsp;|-for循环 （for 循环求和小程序源码、汇编、机器码文件夹）  
&emsp;&emsp;&emsp;|-Feibonacci  （斐波那契小程序源码、汇编、机器码文件夹）  